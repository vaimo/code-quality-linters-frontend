# Vaimo_CodeQualityLintersFrontend

Runner scripts and configuration for different code linters (javascript, css, less) with optional
support to be used with vaimo/code-quality-analyser.

* Package Name: vaimo/code-quality-linters-frontend
* Issue Reports: [HERE](https://bitbucket.org/vaimo/code-quality-linters-frontend/issues)
* Development Workflow: [HERE](https://docs.vaimo.com/workflow-module--v2/guides.html)
