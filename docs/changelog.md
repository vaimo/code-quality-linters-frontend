Changelog
=========

*This file has been auto-generated from the contents of changelog.json*

.. _release-1.2.4:

1.2.4 *(2020-05-27)*
--------------------

.. _release-1.2.4-Fix:

Fix
~~~

* Remove package.json and package-lock.json from gitignore as it prvents them from being added to composer package


.. _release-1.2.3:

1.2.3 *(2020-05-15)*
--------------------

.. _release-1.2.3-Fix:

Fix
~~~

* Fix repo name


.. _release-1.2.2:

1.2.2 *(2020-05-15)*
--------------------

.. _release-1.2.2-Fix:

Fix
~~~

* Fix repo name


.. _release-1.2.1:

1.2.1 *(2020-03-06)*
--------------------

.. _release-1.2.1-Feature:

Feature
~~~~~~~

* Adds GIT CI support


.. _release-1.2.0:

1.2.0
-----

.. _release-1.2.0-Feature:

Feature
~~~~~~~

* Adds suport for eslint configuration imports - changes path to preset path upon eslint copy action


.. _release-1.1.4:

1.1.4 *(2019-10-23)*
--------------------

.. _release-1.1.4-Fix:

Fix
~~~

* Fix style lint autogen marker adding


.. _release-1.1.3:

1.1.3 *(2019-10-23)*
--------------------

.. _release-1.1.3-Fix:

Fix
~~~

* Fix not grabbing default ruleset

.. _release-1.1.3-Maintenance:

Maintenance
~~~~~~~~~~~

* move all preparations code to bootstrap.sh


.. _release-1.1.2:

1.1.2 *(2019-10-06)*
--------------------

.. _release-1.1.2-Fix:

Fix
~~~

* make sure that eslint will check for�the existence for both of .eslintrc.json and .eslintrc so that either one would be used to be used in package root for custom rules

.. _release-1.1.2-Maintenance:

Maintenance
~~~~~~~~~~~

* report the ruleset path used (relative to anaylsed package's root)


.. _release-1.1.1:

1.1.1 *(2019-10-01)*
--------------------

.. _release-1.1.1-Fix:

Fix
~~~

* bad 'no-skip' flag used for eslint which caused an error when code:analyse executed with: --no-skips


.. _release-1.1.0:

1.1.0 *(2019-10-01)*
--------------------

.. _release-1.1.0-Feature:

Feature
~~~~~~~

* provide standardized XML output from all linters (that can be used for scoring and uniform output generation)

.. _release-1.1.0-Fix:

Fix
~~~

* stylelint: report paths not being resolved to navigation directive-free paths
* allow crashes from linters to come through the report generation
* eslint: return code list due to the way linter�execution was contextualized; failures perceived as success (even when failures reported in the log)

.. _release-1.1.0-Maintenance:

Maintenance
~~~~~~~~~~~

* cleanup on npm package configuration (used by Foxy)


.. _release-1.0.1:

1.0.1 *(2019-09-25)*
--------------------

.. _release-1.0.1-Fix:

Fix
~~~

* scripts not including newly included file extensions to the run when added/defined through code-quality.json


.. _release-1.0.0:

1.0.0 *(2019-07-25)*
--------------------

.. note::

    This module is the result of splitting vaimo/code-quality-linters in two => with
    suffixes of -backend and -frontend

.. _release-1.0.0-Feature:

Feature
~~~~~~~

* linter/Fixer: **eslint** - linter for javascript (provided through standardx)
* linter/Fixer: **stylelint** - linter for css/less