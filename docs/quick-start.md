# Quick Start

After installing the package, several binaries will become available to be used with rule-sets that are provided
by the developer.

## eslint

Linter for javascript.

Homepage: https://eslint.org/

### Config

Optionally user can include ruleset file (`.eslintrc` or `.eslintrc.js`) to be present in the root
folder of the package.

Available rules: https://eslint.org/docs/rules

    {
        "rules": {
            "indent": ["error", 4]
        }
    }

### Usage

    # Via code:analyse to run the checks on all files included in analysis
    composer code:analyse --type eslint

    # Via code:analyse to run the checks on specific file/folder
    composer code:analyse --type eslint --file SomeFile.js

    # Via code:normalize to fix the issues on all files included in analysis
    composer code:normalize --type eslint

    # Via code:normalize to fix the issues on specific file/folder
    composer code:normalize --type eslint --file SomeFile.js

    # Directly, to check multiple files (recursively)
    ./vendor/bin/node ./node_modules/eslint/bin/eslint.js 'some/folder/**/*.js'

    # Directly, to fix auto-fixable issues
    ./vendor/bin/node ./node_modules/eslint/bin/eslint.js --fix 'some/folder/**/*.js'

## stylelint

Linter for css/less.

Homepage: https://stylelint.io

### Config

User must include ruleset file (`.stylelintrc`) to be present in the root folder of
the package.

Available rules: https://stylelint.io/user-guide/rules

    {
        "extends": "stylelint-config-recommended",
        "rules": {
            "indentation": [4, {
                "except": ["value"],
                "ignore": ["inside-parens"]
            }]
        }
    }

### Usage

    # Target folder (recursively)
    ./vendor/bin/node ./node_modules/stylelint/bin/stylelint.js \
        'some/folder/**/*.css' \
        'some/folder/**/*.less'

    # Target specific file 
    ./vendor/bin/node ./node_modules/stylelint/bin/stylelint.js SomeFile.css

    # Fix auto-fixable issues
    ./vendor/bin/node ./node_modules/stylelint/bin/stylelint.js --fix 'some/folder/**/*.css'

## prettier

Linter for javascript, html and more.

Homepage: https://prettier.io/

### Config

User must include ruleset file (`.prettierrc`) to be present in the root folder of
the package.

Configuration: https://prettier.io/docs/en/configuration

    {
        "trailingComma": "es5",
        "tabWidth": 4,
        "semi": false,
        "singleQuote": true
    }

### Usage

    # Via code:analyse to run the checks on all files included in analysis
    composer code:analyse --type prettier

    # Via code:analyse to run the checks on specific file/folder
    composer code:analyse --type prettier --file SomeFile.js

    # Via code:normalize to fix the issues on all files included in analysis
    composer code:normalize --type prettier

    # Via code:normalize to fix the issues on specific file/folder
    composer code:normalize --type prettier --file SomeFile.js

    # Directly, to check a folder (recursively)
    ./vendor/bin/node ./node_modules/prettier/bin-prettier.js --check "some/folder/**/*.js"

    # Directly, to check a specific file 
    ./vendor/bin/node ./node_modules/prettier/bin-prettier.js --check SomeFile.js

    # Directly, to fix issues
    ./vendor/bin/node ./node_modules/prettier/bin-prettier.js --write SomeFile.js
