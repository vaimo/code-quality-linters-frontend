# Overview

Runner scripts and configuration for different FRONTEND code linters (javascript, css, less) with optional
support to be used with vaimo/code-quality-analyser.

## Features

* Linter/Fixer: **eslint** - linter for javascript (https://eslint.org/)
* Linter/Fixer: **stylelint** - linter for css/less (https://stylelint.io)
* Linter/Fixer: **prettier** - linter for javascript, html, etc (https://prettier.io/)

