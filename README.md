# Vaimo_CodeQualityLintersFrontend

Runner scripts and configuration for different code linters (javascript, css, less) with optional
support to be used with vaimo/code-quality-analyser.

## Features

* Linter/Fixer: **eslint** - linter for javascript (https://eslint.org/)
* Linter/Fixer: **stylelint** - linter for css/less (https://stylelint.io)
* Linter/Fixer: **prettier** - linter for javascript, html, etc (https://prettier.io/)

## Development

This module uses change-log driver releases. This means that publishing new releases for 
the module is done by the Build Server (CI, Jenkins) and developer just expresses their 
intent on creating a new release by adding a new record to the change-log. 

The guides on how to work with this module can be found [HERE](https://docs.vaimo.com/workflow-module--v2/guides.html).

When using this module as a dependency, consider the following [HINTS](https://docs.vaimo.com/workflow-module--v2/guides.html#dependencies).
