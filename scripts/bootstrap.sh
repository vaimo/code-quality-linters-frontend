#!/bin/bash
: <<'COPYRIGHT'
 Copyright (c) Vaimo Group. All rights reserved.
 See LICENSE_VAIMO.txt for license details.
COPYRIGHT

autogen_marker="\"VAIMO-CODE-QUALITY-AUTOGENERATED\""
node_path="${VCQ_LINTER_VENDOR}/bin/node"

function init {

    prepareNode

    prepareRuleset "$1"

    prepareFileFilter
}

function prepareNode {
    if [ ! -f ${node_path} ] ; then
        if [ "$(which node)" != "" ] ; then
            node_path="node"
        else
            _tt_error "Could not locate NodeJs binary"

            exit 1
        fi
    fi
}

function prepareRuleset {

    if [ "$1" == 'eslint' ]; then
        if [ -f "${VCQ_CONFIG_ROOT}/.eslintrc" ] || [ -f "${VCQ_PRESET_PATH}/.eslintrc" ]; then
            ruleset_name='.eslintrc'
        elif [ -f "${VCQ_CONFIG_ROOT}/.eslintrc.js" ] || [ -f "${VCQ_PRESET_PATH}/.eslintrc.js" ]; then
            ruleset_name='.eslintrc.js'
        else
            ruleset_name='.eslintrc'
        fi
    fi

    if [ "$1" == 'stylelint' ]; then
        ruleset_name='.stylelintrc'
    fi

    if [ "$1" == 'prettier' ]; then
        ruleset_name='.prettierrc'
    fi

    # project level rules file
    ruleset_path="${VCQ_CONFIG_ROOT}/${ruleset_name}"

    if [ ! -f "${ruleset_path}" ] || [ "$(cat ${ruleset_path}|grep ${autogen_marker})" != "" ] ;
    then
        perform_cleanup=1

        rm ${ruleset_path} 2>/dev/null

        # copy rules from presets with added Autogen Marken
        js_ruleset_config_path="${VCQ_PRESET_PATH}/${ruleset_name}"

        if [ "$1" == 'eslint' ]; then
            copyEsLint
        fi

        if [ "$1" == 'stylelint' ]; then
            copyStyleLint
        fi

        _tt_verbose_warning "Using default rules: ${js_ruleset_config_path}"
    else
        _tt_verbose_warning "Using Project level rules: ${ruleset_path}"
    fi
}

function copyStyleLint {
    sed "/^{/a\\
        \"name\": ${autogen_marker},
        " ${js_ruleset_config_path} > ${ruleset_path}
}

function copyEsLint {
    sed "1i\\
        // Name: ${autogen_marker};
        s|\./eslint/|${VCQ_PRESET_PATH}/eslint/|g;
        " ${js_ruleset_config_path} > ${ruleset_path}
}

function prepareFileFilter {
files_filter=$(echo "${VCQ_FILES_FILTER}"|sed 's/,/ /g')

    oIFS=${IFS}
    IFS=' '

    set -f

    _files_filter=

    extensions=$(echo ${VCQ_FILE_TYPES}|sed 's/,/ /g')

    for extension in ${extensions} ; do
        for filter_item in ${files_filter} ; do
            if [ -d ${filter_item} ] ; then
                filter_item=${filter_item}'/**/*.js'
            fi

            _files_filter="${_files_filter} '${filter_item}'"
        done
    done

    files_filter="${_files_filter}"

    set +f
    IFS=${oIFS}
}
