#!/bin/bash
: <<'COPYRIGHT'
 Copyright (c) Vaimo Group. All rights reserved.
 See LICENSE_VAIMO.txt for license details.
COPYRIGHT

source ${VCQ_RUNNER_ROOT}/scripts/utils/notifications.sh
source ${VCQ_RUNNER_ROOT}/scripts/utils/misc.sh
source "$(dirname ${BASH_SOURCE[0]})"/../bootstrap.sh

with_errors=

# print add new line
_tt_verbose_warning ""

# function exports 'node_path', 'files_filter', 'perform_cleanup', 'ruleset_path' variables
init 'stylelint'

bin_path="${node_path} ${VCQ_LINTER_ANCESTOR}/node_modules/stylelint/bin/stylelint.js"

warning=$(tput setaf 3)
reset=$(tput sgr0)
        
if [ "${VCQ_COMMAND}" != "" ] ; then
    cmd="${bin_path} ${VCQ_COMMAND}"
else
    options=
    
    if [ "${VCQ_NO_SKIPS}" != "" ] ; then
        options="${options} --ignore-disables"
    fi

    cmd="${bin_path}${options} ${files_filter} 2>&1"
fi

post_processor=

if [ "${VCQ_RAW_MODE}" != "1" ] ; then
    post_processor="|sed 's#.*${VCQ_CONFIG_ROOT}/##g'"
fi
    
cmd="${cmd}${post_processor}"

if ! _tt_exec "${cmd}" ; then
    with_errors="1"
fi
    
if [ "${VCQ_NO_CLEANUP}" == "" ] && [ "${perform_cleanup}" != "" ] ; then
    rm ${ruleset_path} 2>/dev/null
fi

if [ "${with_errors}" != "" ] ; then    
    exit 1
fi

exit 0
