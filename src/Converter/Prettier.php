<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\CodeQualityLintersFrontend\Converter;

class Prettier implements \Vaimo\CodeQualityAnalyser\Interfaces\ContentConverterInterface
{
    /**
     * @inheritDoc
     */
    public function processContent($output)
    {
        // Not possible to currently really process the output since the only details you get are this:
        // [warn] path/to/file.js
        // So raw output is already enough, we can't parse it in meaningful way like the others

        return $output;
    }
}
