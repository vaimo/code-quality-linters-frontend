<?php

declare(strict_types = 1);

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\CodeQualityLintersFrontend\Converter;

class Eslint implements \Vaimo\CodeQualityAnalyser\Interfaces\ContentConverterInterface
{
    public function processContent($output)
    {
        $filesWithMessages = [];

        $result = [];
        $chunks = \explode(PHP_EOL, $output);
        foreach ($chunks as $chunk) {
            $files = \json_decode($chunk, true) ?? [];
            foreach ($files as $file) {
                $filePath = $file['filePath'] ?? '';
                $messages = $file['messages'] ?? [];

                if (!$messages) {
                    continue;
                }

                $filesWithMessages[] = $file;
                foreach ($messages as $message) {
                    $result[] = [
                        'type' => 'error',
                        'path' => $filePath,
                        'row' => $message['line'],
                        'col' => $message['column'],
                        'code' => $message['ruleId'],
                        'severity' => $message['severity'],
                        'fixable' => \array_key_exists('fix', $message),
                        'comment' => $message['message'],
                        'length' => 1
                    ];
                }
            }
        }

        if (!$result && \trim($output) && $filesWithMessages) {
            return $output;
        }

        return $result;
    }
}
