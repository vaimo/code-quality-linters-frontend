<?php declare(strict_types = 1);
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
namespace Vaimo\CodeQualityLintersFrontend\Converter;

class Stylelint implements \Vaimo\CodeQualityAnalyser\Interfaces\ContentConverterInterface
{
    public function processContent($output)
    {
        $result = [];

        $lines = explode(PHP_EOL, $output);

        $path = '';
        
        $patternPath = '/^[\.a-zA-Z]/';
        $detailsPath = '/(?P<row>[0-9]+):(?P<col>[0-9]+)\s.*?\s(?P<description>[a-zA-Z].*)/';
        
        foreach ($lines as $line) {
            if (!trim($line)) {
                continue;
            }
            
            if (preg_match($patternPath, $line)) {
                $path = realpath(trim($line));
                
                continue;
            }
            
            if (!preg_match($detailsPath, $line, $match)) {
                continue;
            }
            
            $descriptionParts = explode(' ', rtrim($match['description']));
            
            $match['code'] = array_pop($descriptionParts);
            $match['comment'] = trim(implode(' ', $descriptionParts));
            
            $result[] = [
                'type' => 'error',
                'path' => $path,
                'row' => $match['row'],
                'col' => $match['col'],
                'code' => $match['code'],
                'severity' => 5,
                'fixable' => 0,
                'comment' => $match['comment'],
                'length' => 1
            ];
        }

        if (!$result && trim($output)) {
            return $output;
        }

        return $result;
    }
}
